(function ($) {

    'use strict';

    var handleRequest = function () {
        var hash = window.location.hash;
        var view = hash ? hash.replace('#', '') : 'home';

        $.when($.ajax('pages/' + view + '.html')).then(function (data, textStatus, jqXHR) {
            renderView(view, data);
            
        });
    }

    var renderView = function (view, content) {
        $('.page').attr('class', 'page page-' + view);
        $('.page-main').html(content);

        $('.video').each(function (index, item) {
            var $video = $(item);

            if ($video.data('video-cover')) {
                $video.append('<img class="video-cover embed-responsive-item" src="' + $video.data('video-cover') + '">');
            }

            $video.addClass('embed-responsive embed-responsive-16by9');
            $video.append('<a class="video-play embed-responsive-item" href="javascript:void(0)"><i class="icon icon-play-circle-o"></i></a>');

            $video.find('.video-play').on('click', function () {
                $video.addClass('active');
                $video.html('<iframe class="embed-responsive-item" src="//www.youtube.com/embed/' + $video.data('video') + '?rel=0&autoplay=1" allowfullscreen></iframe>');
            });
        });
    }


    handleRequest();

    $(window).on('hashchange', handleRequest);

})(jQuery);
